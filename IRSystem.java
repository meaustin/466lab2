import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.Reader;
import java.util.*;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.json.simple.parser.*;

public class IRSystem {
	HashMap<String, Integer> vocabulary = new HashMap<String, Integer>(); //doc freq
	ArrayList<HashMap<String, Integer>> termFreq = new ArrayList<HashMap<String, Integer>>(); //term freq
	JSONArray json = null;
	ArrayList<Utterance> utterances = new ArrayList<Utterance>();
	
	public IRSystem(File file) {
		try {
			json = (JSONArray) JSONValue.parse(new FileReader(file));
			createUtterances();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	public Utterance getUtter(int index) {
		return utterances.get(index);
	}
	
	private void createUtterances() {
		Iterator i = json.iterator();
		while(i.hasNext()) {
			utterances.add(new Utterance((JSONObject) i.next()));
		}
	}
	
	public void listJSON() {
		for(Utterance utter : utterances) {
			System.out.println(utter.first);
		}
	}
	
	
	public void addToVocab(String str) {
		if(vocabulary.get(str) == null) {
			vocabulary.put(str, 1);
		} else {
			vocabulary.put(str, vocabulary.get(str) + 1);
		}
	}

	private double idf(String str) {
		return (Math.log((0.0 + termFreq.size()) / vocabulary.get(str)) / Math.log(2));
	}

	private double tfidfWeight(String term, HashMap<String, Integer> document) {
		return document.get(term) * vocabulary.get(term);
	}

	private double tfidfWeightShort(HashMap<String, Integer> document, String term) {
		return (0.5 + 0.5 * document.get(term)) * vocabulary.get(term);
	}

	private double cosineSim(HashMap<String, Integer> document, List<String> terms) {

		return 0.0;
	}
}