/*
TEAM MEMBERS:
Matthew Austin
Gabriel Hernandez
*/

import java.util.*;
import java.lang.*;
import java.io.*;

public class WordVector {
	ArrayList<Double[]> rows = null;
	public WordVector() {
		rows = new ArrayList<Double[]>();
	}
	public WordVector(File file) {
		rows = new ArrayList<Double[]>();
		parseCSV(file);
	}
	public void parseCSV(File file) {
		Scanner rowScanner = null;
		Scanner colScanner = null;
		int colLength = 0;
		try {
			rowScanner = new Scanner(file).useDelimiter("\n");

		} catch (Exception e) {
			return;
		}
		while(rowScanner.hasNext()){
			String rowString = rowScanner.next();
			String[] splitString = rowString.split(",");
			colLength = splitString.length;
			if(splitString.length == colLength) {
				Double[] curCol = new Double[colLength];
				rows.add(curCol);
				for(int i = 0; i < splitString.length; i++) {
					if(splitString[i].equals("")) {
						curCol[i] = 0.0;
					} else {
						curCol[i] = Double.parseDouble(splitString[i]);
					}
				}
			} else {
				System.out.println("Error");
				//throw error
			}
		}
	}

	public double length(int vectorIndex) {
		Double[] vector = rows.get(vectorIndex);
		double sum = 0.0;
		for(int i = 0; i < vector.length; i++) {
			sum += Math.pow(vector[i], 2);
		}
		return Math.sqrt(sum) ;
	}

	public double dotProduct(int vectorIndex1, int vectorIndex2) {
		Double[] vector1 = rows.get(vectorIndex1);
		Double[] vector2 = rows.get(vectorIndex2);
		double sum = 0;
		if(vector1.length != vector2.length){
			throw new IllegalArgumentException("The two vectors must have the same length");
		}
		for(int i = 0; i < vector1.length; i++){
			sum += vector1[i] * vector2[i];
		}
		return sum;
	}

	public double eDistance(int vectorIndex1, int vectorIndex2) {
		Double[] vector1 = rows.get(vectorIndex1);
		Double[] vector2 = rows.get(vectorIndex2);
		double sum = 0.0;
		if(vector1.length != vector2.length){
			throw new IllegalArgumentException("The two vectors must have the same length");
		}
		for(int i = 0; i < vector1.length; i++){
			sum += Math.pow(vector1[i] - vector2[i],2);
		}
		return Math.sqrt(sum);
	}

	public double mDistance(int vectorIndex1, int vectorIndex2) {
		Double[] vector1 = rows.get(vectorIndex1);
		Double[] vector2 = rows.get(vectorIndex2);
		double sum = 0.0;
		if(vector1.length != vector2.length){
			throw new IllegalArgumentException("The two vectors must have the same length");
		}
		for(int i = 0; i < vector1.length; i++){
			sum += Math.abs(vector1[i] - vector2[i]);
		}
		return sum;
	}

	public double mean(int vectorIndex1) {
		Double[] vector1 = rows.get(vectorIndex1);
		double sum = 0.0;
		for (int i = 0; i < vector1.length; i++) {
			sum += vector1[i];
		}
		return sum / vector1.length;
	}

	private double mean(Double[] vector) {
		Double[] vector1 = vector;
		double sum = 0.0;
		for (int i = 0; i < vector1.length; i++) {
			sum += vector1[i];
		}
		return sum / vector1.length;
	}

	public double min(int vectorIndex1) {
		double minValue = Integer.MAX_VALUE;
		Double[] vector1 = rows.get(vectorIndex1);
		for(int i = 0; i < vector1.length; i++) {
			if(vector1[i] < minValue) {
				minValue = vector1[i];
			}
		}
		return minValue;
	}

	public double max(int vectorIndex1) {
		double maxValue = Integer.MIN_VALUE;
		Double[] vector1 = rows.get(vectorIndex1);
		for(int i = 0; i < vector1.length; i++) {
			if(vector1[i] > maxValue) {
				maxValue = vector1[i];
			}
		}
		return maxValue;
	}

	public double colMedian(int col) {
		Double[] newArray = new Double[rows.size()];
		double value = 0.0;
		for(int i = 0; i < rows.size(); i++) {
			if(col >= rows.get(i).length) {
				throw new RuntimeException("Vector doesn't have this column");
			}
			newArray[i] = rows.get(i)[col];
		}
		Arrays.sort(newArray);
		if(newArray.length % 2 == 0) {
			value =  (newArray[(newArray.length / 2)] + newArray[(newArray.length / 2) - 1]) / 2;
		} else {
			value =  newArray[newArray.length / 2];
		}
		return value;

	}

	public double stdDeviation(int vectorIndex1) {
		Double[] vector1 = rows.get(vectorIndex1);
		double sum = 0.0;
		double mean = mean(vectorIndex1);
		for(int i = 0; i < vector1.length; i++) {
			sum += Math.pow((vector1[i] - mean), 2);
		}
		sum /= 	vector1.length;
		return Math.sqrt(sum);
	}

	private double stdDeviation(Double[] vector) {
		Double[] vector1 = vector;
		double sum = 0.0;
		double mean = mean(vector1);
		for(int i = 0; i < vector1.length; i++) {
			sum += Math.pow((vector1[i] - mean), 2);
		}
		sum /= vector1.length;
		return Math.sqrt(sum);
	}

	public double stdDeviationCol(int col) {
		Double[] vector = new Double[rows.size()];
		for(int i = 0; i < rows.size(); i++) {
			if(col >= rows.get(i).length) {
				throw new RuntimeException("Vector doesn't have this column");
			}
			vector[i] = rows.get(i)[col];
		}
		return stdDeviation(vector);
	}

	public double pCorrelation(int vectorIndex1, int vectorIndex2) {
		Double[] vector1 = rows.get(vectorIndex1);
		Double[] vector2 = rows.get(vectorIndex2);
		double mean1 = mean(vectorIndex1);
		double mean2 = mean(vectorIndex2);
		double cov = 0.0;
		if(vector1.length != vector2.length){
			throw new IllegalArgumentException("The two vectors must have the same length");
		}
		for(int i = 0; i < vector1.length; i++) {
			cov += (vector1[i] - mean1) * (vector2[i] - mean2);
		}
		cov /= vector1.length;
		return (cov / (stdDeviation(vectorIndex1) * stdDeviation(vectorIndex2)));
	}

	public double colMin(int col) {
		double minValue = Integer.MAX_VALUE;
		double value = 0.0; 
		for(int i = 0; i < rows.size(); i++) {
			if(col >= rows.get(i).length) {
				throw new RuntimeException("Vector doesn't have this column");
			}
			value = rows.get(i)[col];
			if(value < minValue) {
				minValue = value;
			}
		}
		return minValue;
	}

	public double colMax(int col) {
		double maxValue = Integer.MIN_VALUE;
		double value = 0.0; 
		for(int i = 0; i < rows.size(); i++) {
			if(col >= rows.get(i).length) {
				throw new RuntimeException("Vector doesn't have this column");
			}
			value = rows.get(i)[col];
			if(value > maxValue) {
				maxValue = value;
			}
		}
		return maxValue;
	}

	public double colMean(int col) {
		double meanValue = 0.0;
		double value = 0.0; 
		for(int i = 0; i < rows.size(); i++) {
			if(col >= rows.get(i).length) {
				throw new RuntimeException("Vector doesn't have this column");
			}
			meanValue += rows.get(i)[col];
		}
		return meanValue / rows.size();
	}

	public double median(int vectorIndex) {
		Double[] vector = rows.get(vectorIndex);
		double value = 0.0;
		for(int i = 0; i < vector.length; i++) {
			vector = Arrays.copyOf(vector, vector.length);
			Arrays.sort(vector);
			if(vector.length % 2 == 0) {
				value =  (vector[(vector.length / 2)] + vector[(vector.length / 2) - 1]) / 2;
			} else {
				value =  vector[vector.length / 2];
			}
		}
		return value;
	}
}