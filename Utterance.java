import org.json.simple.JSONObject;

public class Utterance {
	public long pid;
	public String first, last, personType, date, house, committee, text;
	public Utterance(JSONObject json) {
		pid = (Long) json.get("pid");
		first = (String) json.get("first");
		last = (String) json.get("last");
		personType = (String) json.get("PersonType");
		date = (String) json.get("date");
		house = (String) json.get("house");
		committee = (String) json.get("Committee");
		text = (String) json.get("text");
	}
}
